<?php

function getConnection() { //get database connection
    //Connect to the database
    $host = "eseamons-html_workspace-1625130";   //See Step 3 about how to get host name
    $user = "eseamons";                         //Your Cloud 9 username
    $pass = "";                                 //Remember, there is NO password!
    $db = "maps_db";                          //Your database name you want to connect to
    $port = 3306;                               //The port #. It is always 3306

    $connection = mysqli_connect($host, $user, $pass, $db, $port)or die(mysql_error());
    
    return $connection;
}

function initialize_page() {
   $connection = getConnection();
            
   $select = '
                <select onchange="setMap()" id="countries">
                    
            
             ';
            
            
    $query = "SELECT * FROM locations ORDER BY country ASC";
    $result = mysqli_query($connection, $query);
    
    
    while ($row = mysqli_fetch_assoc($result)) {
          $select .= '<option " value="'.$row['latitude'].','.$row['longitude'].'">'.$row['city'].', '.$row['country'].'</option>';
    }
    
            
    $select .= '        
                </select>
                <br>
                <br>
                
                <select onchange="setMapSize()" id="size">
                    <option  value="500,380">Small Map</option>
                    <option selected="selected" value="750,570">Big Map</option>
                </select>
                <br>
                <br>
              ';
             
    echo $select;
}
//<option value="" disabled="disabled" selected="selected">Please select a name</option>
?>