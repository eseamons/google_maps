<!DOCTYPE html>
<html>
    <head>
        <title>Edit Locations</title>
    </head>
    <body>
        <?php
            include('functions.php');
            $connection = getConnection();
            $query = "SELECT * FROM locations ORDER BY country ASC";
            $result = mysqli_query($connection, $query);
            
            echo "<h1 style='color:green' align='center'>Edit Location information</h1>
                 <br>
                 <a href='https://html-workspace-eseamons.c9.io/googleMaps'>Return to Maps Page</a>
                 <br>
                 <br>
                 <br>";
            $i = 0;
            while ($row = mysqli_fetch_assoc($result)) {
                $i++;
                echo "<Form name ='form$i' Method ='POST' Action ='submitChanges.php'>";
                echo $row['city'].", ".$row['country'].': <br>
                <INPUT TYPE = "TEXT" VALUE ="'.$row['id'].'" Name ="id" READONLY>
                <INPUT TYPE = "TEXT" VALUE ="'.$row['city'].'" Name ="city">
                <INPUT TYPE = "TEXT" VALUE ="'.$row['country'].'" Name ="country">
                <INPUT TYPE = "TEXT" VALUE ="'.$row['latitude'].'" Name ="latitude">
                <INPUT TYPE = "TEXT" VALUE ="'.$row['longitude'].'" Name ="longitude">
                ';
                echo "<INPUT TYPE = 'Submit' Name = 'Submit$i' VALUE = 'Submit Changes'>
                      <br>
                      <br>
                      ";
                echo '</form>';
            }
            
        ?>
    </body>
</html>