<!DOCTYPE html>
<html>
    <head>
        <title>Map Location Selector</title>
        <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script> 
        
        <script>
           /*global google*/
            
            google.maps.event.addDomListener(window, 'load', setMap);
            
            function setMap() {
                
                var object = document.getElementById("countries")
                var selIndex = object.selectedIndex
                document.getElementById("ptext").innerHTML = object.options[selIndex].value; //http://www.mredkj.com/tutorials/tutorial002.html
                var value = object.options[selIndex].value
                var nums = value.split(",");
                var myCenter=new google.maps.LatLng(nums[0],nums[1]);
                var mapProp = {
                  center:myCenter,
                  zoom:5,
                  mapTypeId:google.maps.MapTypeId.ROADMAP
                  };
                
                var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
                
                var marker=new google.maps.Marker({
                  position:myCenter,
                  });
                
                marker.setMap(map);
            }
            
            function setMapSize() {
                 var object = document.getElementById("size");
                 var selIndex = object.selectedIndex;
                 var value = object.options[selIndex].value;
                 var nums = value.split(",");
                 document.getElementById("googleMap").style.width = nums[0] + "px";
                 document.getElementById("googleMap").style.height = nums[1] + "px";
            }
            
        </script>
    </head>
    <body>
        <?php
              include('functions.php');
              initialize_page();
        ?>
    <div id="googleMap" style="width:750px;height:570px;"></div>
    
        <?php
            echo ' <p id="ptext"></p>';
        ?>
        <br>
        <button onclick="setMap()">Reset Map</button>
        <br>
        <br>
        <button onclick="gotoAddLocation()">Add Location</button>
        <br>
        <br>
        <button onclick="gotoEditLocations()">Edit Locations</button>
        <script>
            function gotoAddLocation() {
                window.location.href = "https://html-workspace-eseamons.c9.io/googleMaps/addLocation.html";
            }
            function gotoEditLocations() {
                window.location.href = "https://html-workspace-eseamons.c9.io/googleMaps/editLocations.php";
            }
        </script>
    </body>
</html>