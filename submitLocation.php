<!DOCTYPE html>
<html>
    <head>
        <title>Submit Page</title>
    </head>
    <body>
        <?php

            include('functions.php');
            $connection = getConnection();
            
            $city = $_POST['city'];
            $country = $_POST['country'];
            $latitude = $_POST['latitude'];
            $longitude = $_POST['longitude'];
            
            $valid = TRUE;
            
            if($city == "" || $country == "" || $latitude == "" || $longitude == "") {
                $valid = FALSE;
            }
            
            if($city == "city" || $country == "country" || $latitude == "latitude" || $longitude == "longitude") {
                $valid = FALSE;
            }
            
            
            if($valid) {
                $query = "INSERT INTO locations (city,country,latitude,longitude) VALUES ('".$city."','".$country."','".$latitude."','".$longitude."')";
                $result = mysqli_query($connection, $query);
                
                if(!$result){
                    echo '<p>The form submition was not successful</p>';
                }
                else {
                    echo '<p>City: '.$city.'</p>';
                    echo '<p>Country: '.$country.'</p>';
                    echo '<p>Latitude: '.$latitude.'</p>';
                    echo '<p>Longitude: '.$longitude.'</p>';
                    echo '<p>Query: '.$query.'</p><br><br>';
                }
                
            }
            else {
                echo "You didn't give a valid input";
            }
        ?>
        <a href="https://html-workspace-eseamons.c9.io/googleMaps/">Return to Maps</a>
    </body>
</html>